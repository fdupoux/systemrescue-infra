# SystemRescue Infrastructure as Code

## Overview
The code in this repository is used to manage the infrastructure of the
SystemRescue project.

## Ansible
Servers are configured using Ansible. Each playbook applies a single top-level
role. A top-level roles correspond to a particular type of server, such as
"web server" or "git server". The names of top-level roles start with the
prefix "main-". Each top-level role includes multiple ordinary roles. Each
ordinary role provides support for a particular aspect of the system, such
as "iptables", "nginx", or "system user accounts".

The code of the roles should remain generic and parameterized using variables
named after the role. Specific details such as user names and addresses should
be defined in group_vars.

## Secrets
The code in this repository depends on some sensitive information, which is
stored in a git-crypt encrpyted repository which is accessed as a git submodule.
